import { MoneyPipe } from './money.pipe';

describe('MoneyPipe', () => {
  let inputValue = 2000;

  it('create an instance of the pipe moneyCustomPipe', () => {
    const pipe = new MoneyPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform value to add "Rs" at the start of the number', () => {
    const pipe = new MoneyPipe();
    const result = pipe.transform(inputValue);
    expect(result).toEqual('Rs 2000');
  })

  it('should check that value has been transformed and "Rs" has not been omitted at the start of the number', () => {
    const pipe = new MoneyPipe();
    const result = pipe.transform(inputValue);
    expect(result).not.toEqual('2000');
  })
});
