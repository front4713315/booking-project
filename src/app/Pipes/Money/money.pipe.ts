import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyCustomPipe',
})
export class MoneyPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    return 'Rs ' + value;
  }
}
