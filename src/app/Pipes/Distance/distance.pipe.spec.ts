import { DistancePipe } from './distance.pipe';

describe('DistancePipe', () => {
  let inputValue = 5000;

  it('create an instance of the pipe distanceCustomPipe', () => {
    const pipe = new DistancePipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform value to add "km" at the end of the number', () => {
    const pipe = new DistancePipe();
    const result = pipe.transform(inputValue);
    expect(result).toEqual('5000 km');
  })

  it('should check that value has been transformed and "km" has not been omitted at the end of the number', () => {
    const pipe = new DistancePipe();
    const result = pipe.transform(inputValue);
    expect(result).not.toEqual('5000');
  })
});
