import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'distanceCustomPipe',
})
export class DistancePipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    return value + ' km';
  }
}
