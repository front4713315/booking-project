import { NgModule } from '@angular/core';
import { AuthGuard } from './Auth-Guards/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { FlightDetailsComponent } from './Components/flight-details/flight-details.component';
import { MyReservationComponent } from './Components/my-reservation/my-reservation.component';
import { NewReservationComponent } from './Components/new-reservation/new-reservation.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'my-reservation', component: MyReservationComponent, canActivate: [AuthGuard]}, 
  { path: 'new-reservation/:flightId', component: NewReservationComponent, canActivate: [AuthGuard]},
  { path: 'flight-details', component: FlightDetailsComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
