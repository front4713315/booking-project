import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const IdenticalPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  let password = control.get('password');
  let confirmPassword = control.get('confirmPassword');
  return (password && confirmPassword && password?.value != confirmPassword?.value) ? { identicalPasswordValidator: true, } : null;
};
