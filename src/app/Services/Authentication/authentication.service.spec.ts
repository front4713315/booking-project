import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AuthenticationService', () => {
  let authenticationService: AuthenticationService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdG5hbWUiOiJqb2huIiwibGFzdG5hbWUiOiJkb2UiLCJpc0xvZ2dlZEluIjp0cnVlLCJlbWFpbCI6ImpvaG5kb2VAZW1haWwuY29tIn0.DyfiZ1nFjHdnFtkaVfiprbXt7pkpB8blwF5AETtFWq4'
  let TOKEN_DECODED = {
    firstname: 'john',
    lastname: 'doe',
    isLoggedIn: true,
    email: 'johndoe@email.com'
  };
  let SIGN_IN_RESPONSE = [
    {
      id: 1,
      email: 'johndoe@email.com',
      statusCode: 200,
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdG5hbWUiOiJqb2huIiwibGFzdG5hbWUiOiJkb2UiLCJpc0xvZ2dlZEluIjp0cnVlLCJlbWFpbCI6ImpvaG5kb2VAZW1haWwuY29tIn0.DyfiZ1nFjHdnFtkaVfiprbXt7pkpB8blwF5AETtFWq4'
    }
  ];
  let POST_PAYLOAD =  {
    firstname: "jane",
    lastname: "doe",
    password: "***",
    emailAddress: "janedoe@email.com"
  };
  let POST_RESPONSE = [
    {
      id: 1,
      email: 'janedoe@email.com',
      statusCode: 200,
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdG5hbWUiOiJqYW5lIiwibGFzdG5hbWUiOiJkb2UiLCJpc0xvZ2dlZEluIjp0cnVlLCJlbWFpbCI6ImphbmVkb2VAZW1haWwuY29tIn0.s-4TNto4QGR9oypMpMDZNC7SQyLyikiTDVASO3VFrwU'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[ HttpClientTestingModule ]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    authenticationService = new AuthenticationService(httpClientSpy)
  });

  it('should create an instance of the authentication service', () => {
    expect(authenticationService).toBeTruthy();
  });

  it("should decode a token and return all its details when decodeToken() is executed", () => {
    sessionStorage.setItem('token', TOKEN)
    spyOn(authenticationService, 'decodeToken').and.returnValue(TOKEN_DECODED);
    let tokenDetails = authenticationService.decodeToken();
    expect(tokenDetails).toBeTruthy();
    expect(tokenDetails).toEqual(TOKEN_DECODED)
  });

  it("should return the value of a keyname from a token when getTokenDetails() is executed", () => {
    spyOn(authenticationService, 'decodeToken').and.returnValue(TOKEN_DECODED);
    let isLoggedInDetails = authenticationService.getTokenDetails('isLoggedIn');
    expect(isLoggedInDetails).toBeTruthy();
    expect(isLoggedInDetails).toEqual(TOKEN_DECODED['isLoggedIn'])
    let nameDetails = authenticationService.getTokenDetails('name');
    expect(nameDetails).toBeTruthy();
    expect(nameDetails).toEqual(TOKEN_DECODED['firstname'] + ' ' + TOKEN_DECODED['lastname'])
    let emailDetails = authenticationService.getTokenDetails('email');
    expect(emailDetails).toBeTruthy();
    expect(emailDetails).toEqual(TOKEN_DECODED['email'])
  })

  it("should return the user's log in details when getUserLogin() is executed", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(SIGN_IN_RESPONSE));
    authenticationService.getUserLogin().subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(SIGN_IN_RESPONSE);
        done();
      }
    )
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it("should return a new user's log in details when postNewUserSignUp() is executed to sign up a new user", (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(POST_RESPONSE));
    authenticationService.postNewUserSignUp(POST_PAYLOAD).subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(POST_RESPONSE);
        done();
      }
    )
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
  });
});
