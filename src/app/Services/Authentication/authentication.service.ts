import { Observable } from 'rxjs';
import { jwtDecode } from 'jwt-decode';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SignInResponse } from 'src/app/Models/signInResponse.model';

interface myToken {
  firstname: string;
  lastname: string;
  isLoggedIn: boolean;
  email: string
}

@Injectable({providedIn: 'root',})
export class AuthenticationService {
  _API_MOCK_URL: string = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient) {}

  decodeToken(): myToken {
    if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') != '') {
      const token = sessionStorage.getItem('token') || '';
      const decoded = jwtDecode<myToken>(token);
      return decoded;
    }
    return { firstname: '', lastname: '', isLoggedIn: false, email: '' };
  }

  getTokenDetails(keyName: string): any {
    let decodedToken = this.decodeToken();
    if(keyName === 'isLoggedIn') {
      return decodedToken['isLoggedIn']
    }
    if(keyName === 'name') {
      return decodedToken['firstname'] + ' ' + decodedToken['lastname'];
    }
    if(keyName === 'email') {
      return decodedToken['email'];
    }
    return null;
  }

  getUserLogin(): Observable<SignInResponse[]> {
    return this.httpClient.get<SignInResponse[]>(this._API_MOCK_URL + 'sign-in-response/?id=1');
  }

  postNewUserSignUp(body: any): Observable<any> {
    return this.httpClient.post<any[]>(this._API_MOCK_URL + 'users', body);
  }
}
