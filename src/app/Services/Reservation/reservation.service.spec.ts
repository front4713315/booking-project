import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { ReservationService } from './reservation.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ReservationService', () => {
  let reservationService: ReservationService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let RESERVATION_GET_MOCK_DATA = [
    {
      id: 1,
      email: "johndoe@email.com",
      bookedBy: "john doe",
      dateOfBooking: new Date("2024-02-24T05:59:40.101Z"),
      flightId: 1,
      flightName: "M-AIR 172",
      numberOfSeatsBooked: { adult: 1, child: 2 },
      totalCost: 23970,
      countryOfDeparture: "Mauritius",
      countryOfArrival: "Rodrigues",
      dateOfDeparture: new Date("2024-03-05T06:21:40.101Z")
    }
  ]; 
  let RESERVATION_POST_MOCK_DATA = {
    email: "johndoe@email.com",
    bookedBy: "john doe",
    dateOfBooking: new Date("2024-02-24T05:59:40.101Z"),
    flightId: 1,
    flightName: "M-AIR 172",
    numberOfSeatsBooked: { adult: 1, child: 2 },
    totalCost: 23970,
    countryOfDeparture: "Mauritius",
    countryOfArrival: "Rodrigues",
    dateOfDeparture: new Date("2024-03-05T06:21:40.101Z")
  };
  let RESERVATION_RESPONSE_MOCK_DATA = {
    id: 1,
    email: "johndoe@email.com",
    bookedBy: "john doe",
    dateOfBooking: new Date("2024-02-24T05:59:40.101Z"),
    flightId: 1,
    flightName: "M-AIR 172",
    numberOfSeatsBooked: { adult: 1, child: 2 },
    totalCost: 23970,
    countryOfDeparture: "Mauritius",
    countryOfArrival: "Rodrigues",
    dateOfDeparture: new Date("2024-03-05T06:21:40.101Z")
  };
  const authenticationServiceSpy = jasmine.createSpyObj('authenticationService',['getTokenDetails'])
  authenticationServiceSpy.getTokenDetails.and.returnValue(of('john doe'));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    reservationService = new ReservationService(httpClientSpy, authenticationServiceSpy);
  });

  it('should create an instance of the reservation service', () => {
    expect(reservationService).toBeTruthy();
  });

  it("should return the user's reservation when getMyReservation() is executed", (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(RESERVATION_GET_MOCK_DATA));
    reservationService.getMyReservation().subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(RESERVATION_GET_MOCK_DATA);
        done();
      }
    )
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it('should add a new reservation when postNewReservation() is executed', (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(RESERVATION_RESPONSE_MOCK_DATA));
    reservationService.postNewReservation(RESERVATION_POST_MOCK_DATA).subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(RESERVATION_RESPONSE_MOCK_DATA);
        done();
      }
    )
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
  });
});
