import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reservation } from 'src/app/Models/reservation.model';
import { AuthenticationService } from '../Authentication/authentication.service';

@Injectable({providedIn: 'root',})
export class ReservationService {
  _API_MOCK_URL: string = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) { }

  getMyReservation(): Observable<Reservation[]> {
    return this.httpClient.get<Reservation[]>(this._API_MOCK_URL +'reservation/?bookedBy=' + this.authenticationService.getTokenDetails('name'));
  }

  postNewReservation(reservationDetails: Reservation): Observable<Reservation> {
    return this.httpClient.post<Reservation>(this._API_MOCK_URL + 'reservation', reservationDetails);
  }
}