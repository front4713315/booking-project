import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Destinations } from 'src/app/Models/destinations.model';
import { FlightInformation } from 'src/app/Models/flight-Information.model';

@Injectable({providedIn: 'root',})
export class FlightsService {
  _API_MOCK_URL: string = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient) {}

  getDestination(): Observable<Destinations[]> {
    return this.httpClient.get<Destinations[]>(this._API_MOCK_URL + 'destinations');
  }

  getFlightInformation(): Observable<FlightInformation[]> {
    return this.httpClient.get<FlightInformation[]>(this._API_MOCK_URL + 'flights-information');
  }

  putFlightInformation(flightId: any, payload: FlightInformation): Observable<FlightInformation> {
    return this.httpClient.put<FlightInformation>(this._API_MOCK_URL + 'flights-information/' + flightId, payload);
  }

  getFlightInformationByCriteria(origin: any, destination: any): Observable<FlightInformation[]> {
    return this.httpClient.get<FlightInformation[]>(this._API_MOCK_URL + 'flights-information/?origin=' + origin +'&destination=' + destination);
  }

  getFlightInformationById(flightId: any): Observable<FlightInformation> {
    return this.httpClient.get<FlightInformation>(this._API_MOCK_URL + 'flights-information/' + flightId);
  }
}
