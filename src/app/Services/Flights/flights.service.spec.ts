import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { FlightsService } from './flights.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FlightsService', () => {
  let flightService: FlightsService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let DESTINATIONS_GET_MOCK_DATA =  [
    { id: 1, airport: "SSR International Airport", country: "Mauritius" },
    { id: 2, airport: "Plaine Corail Airport", country: "Rodrigues" },
    { id: 3, airport: "Roland Garros Airport", country: "Réunion Island" }
  ];
  let FLIGHT_BY_ID = {
    id: 1,
    name: "M-AIR 172",
    origin: "Mauritius",
    destination: "Rodrigues",
    capacity: 172,
    availability: 88,
    dateOfDeparture: "2024-04-15T05:59:40.101Z",
    duration: "1 hr 35 minutes",
    distance: "617",
    ticketCostAdult: 9990,
    ticketCostChild: 6990
  };
  let FLIGHT_INFORMATIONS_GET_MOCK_DATA = [
    {
      id: 1,
      name: "M-AIR 172",
      origin: "Mauritius",
      destination: "Rodrigues",
      capacity: 172,
      availability: 88,
      dateOfDeparture: "2024-04-15T05:59:40.101Z",
      duration: "1 hr 35 minutes",
      distance: "617",
      ticketCostAdult: 9990,
      ticketCostChild: 6990
    }
  ];
  let Flight_INFORMATION_PUT_MOCK_DATA = {
    id: 1,
    name: "M-AIR 172",
    origin: "Mauritius",
    destination: "Rodrigues",
    capacity: 172,
    availability: 88,
    dateOfDeparture: "2024-04-15T05:59:40.101Z",
    duration: "1 hr 35 minutes",
    distance: "617",
    ticketCostAdult: 9990,
    ticketCostChild: 6990
  };

  beforeEach(() => {
    TestBed.configureTestingModule({      
      imports: [HttpClientTestingModule]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put']);
    flightService = new FlightsService(httpClientSpy);
  });

  it('should create an instance of the flight service', () => {
    expect(flightService).toBeTruthy();
  });

  it('should return all the destinations available when getDestination() is executed', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(DESTINATIONS_GET_MOCK_DATA))
    flightService.getDestination().subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(DESTINATIONS_GET_MOCK_DATA);
        done();
    })
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it('should return all the flights information when getFlightInformation() is executed', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(FLIGHT_INFORMATIONS_GET_MOCK_DATA ))
    flightService.getFlightInformation().subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(FLIGHT_INFORMATIONS_GET_MOCK_DATA );
        done();
    })
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it("should update an existing flight's information when putFlightInformation() is executed", (done: DoneFn) => {
    httpClientSpy.put.and.returnValue(of(Flight_INFORMATION_PUT_MOCK_DATA ))
    flightService.putFlightInformation(Flight_INFORMATION_PUT_MOCK_DATA['id'], Flight_INFORMATION_PUT_MOCK_DATA).subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(Flight_INFORMATION_PUT_MOCK_DATA);
        done();
      }
    )
    expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
  });

  it("should return a flight's information according to origin and destination when getFlightInformationByCriteria() is executed", (done: DoneFn) => {
    let origin = 'Marutitius';
    let destination = 'Rodrigues';
    httpClientSpy.get.and.returnValue(of(FLIGHT_INFORMATIONS_GET_MOCK_DATA))
    flightService.getFlightInformationByCriteria(origin, destination).subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(FLIGHT_INFORMATIONS_GET_MOCK_DATA);
        done();
    })
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it("should return a flight's information according to the id when getFlightInformationById() is executed", (done: DoneFn) => {
    let FLIGHT_ID = 1;
    httpClientSpy.get.and.returnValue(of(FLIGHT_BY_ID))
    flightService.getFlightInformationById(FLIGHT_ID).subscribe(
      response => {
        expect(response).toBeTruthy();
        expect(response).toEqual(FLIGHT_BY_ID);
        done();
    })
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });
});
