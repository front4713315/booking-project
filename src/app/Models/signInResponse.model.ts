export class SignInResponse {
  constructor(
    public id: any,
    public email: string,
    public statusCode: number,
    public token: string
  ) {}
}