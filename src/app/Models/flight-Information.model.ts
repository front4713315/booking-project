export class FlightInformation {
  constructor(
    public availability: number,
    public capacity: number,
    public dateOfDeparture: any,
    public destination: string,
    public distance: string,
    public duration: string,
    public name: string,
    public origin: string,
    public ticketCostAdult: number,
    public ticketCostChild: number,
    public id?: any
  ) {}
}