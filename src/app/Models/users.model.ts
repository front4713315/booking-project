export class Users {
  constructor(
    public emailAddress: string,
    public firstname: string,
    public lastname: string,
    public password: string,
    public id?: any
  ) {}
}