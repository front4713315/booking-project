export class Reservation {
  constructor(
    public bookedBy: string,
    public countryOfArrival: string,
    public countryOfDeparture: string,
    public dateOfBooking: any,
    public dateOfDeparture: any,
    public email: string,
    public flightId: any,
    public flightName: string,
    public numberOfSeatsBooked: {
      adult: number;
      child: number;
    },
    public totalCost: number,
    public id?: any
  ) {}
}