import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree,} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authenticationService.getTokenDetails('isLoggedIn')) {
      return true;
    } 
    else {
      this.router.navigate(['/home']);
      return false;
    }
  }
}
