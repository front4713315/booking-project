import { AppComponent } from './app.component';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent } from './Components/header/header.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AppComponent', () => {
  let httpMock: HttpClientTestingModule;
  let fixture: ComponentFixture<AppComponent>;
  let element: HTMLElement;
  let component: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule, MatIconModule ],
      declarations: [ AppComponent, HeaderComponent ],
    }).compileComponents();
    httpMock = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {    
    expect(component).toBeTruthy();
  });

  it('shoud check that the title of the project is Project1', () => {
    expect(component.title).toEqual('booking');
  });

  it("should check that html elements exists", () => {
    element = fixture.nativeElement.querySelector('#container');
    expect(element).toBeTruthy();
    element = fixture.nativeElement.querySelector('#main-container');
    expect(element).toBeTruthy();
    element = fixture.nativeElement.querySelector('#router-outlet');
    expect(element).toBeTruthy();
  })
});