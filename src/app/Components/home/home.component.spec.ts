import { HomeComponent } from './home.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { SignInComponent } from '../sign-in/sign-in.component';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let element: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent, SignInComponent, SignUpComponent ],
      imports: [ MatTabsModule, MatIconModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule, HttpClientTestingModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the home component', () => {
    expect(component).toBeTruthy();
  });

  it('shoud check that the title of the page and the variable "title" have the same value', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('#title-line-1');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain(component.title);
  });

  it('shoud check that the subtitle of the page and the variable "subtitle" have the same value', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('#title-line-2');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain(component.subtitle);
  });

  it('should check that mat-tab-group div exist in the DOM to dislay sign in & sign up option', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('#title-line-2');
    expect(element).toBeTruthy()
  })
});
