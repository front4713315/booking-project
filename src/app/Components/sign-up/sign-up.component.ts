import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';
import { IdenticalPasswordValidator } from '../../CustomValidators/identical.password.validator';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss',],
})
export class SignUpComponent {
  passwordError: string = '';
  signUpForm: FormGroup = this.formBuilder.group({
      firstname: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(1)]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
    { validators: IdenticalPasswordValidator,}
  );

  constructor(private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthenticationService) {}

  signUp() {
    if (this.signUpForm.valid) {
      this.authenticationService.postNewUserSignUp(this.constructPayload()).subscribe(
        data => {
          if (data) {
            sessionStorage.setItem('token','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdG5hbWUiOiJqYW5lIiwibGFzdG5hbWUiOiJkb2UiLCJpc0xvZ2dlZEluIjp0cnVlLCJlbWFpbCI6ImphbmVkb2VAZW1haWwuY29tIn0.s-4TNto4QGR9oypMpMDZNC7SQyLyikiTDVASO3VFrwU');
            alert(`User: ${data.firstname} ${data.lastname} successfully created!`);
            this.router.navigate(['flight-details']);
          }
        }
      );
    } 
    else {
      this.signUpForm.reset();
      alert('An error has occured. Please try again!');
    }
  }

  constructPayload() {
    return {
      firstname: this.signUpForm.controls['firstname'].value,
      lastname: this.signUpForm.controls['lastname'].value,
      password: this.signUpForm.controls['password'].value,
      emailAddress: this.signUpForm.controls['email'].value,
    };
  }

  isPasswordSame() {
    this.passwordError = (this.signUpForm.controls['password'].value && this.signUpForm.controls['confirmPassword'].value && this.signUpForm.controls['password'].value !=this.signUpForm.controls['confirmPassword'].value) ? 'Passwords do not match' : '';
  }
}
