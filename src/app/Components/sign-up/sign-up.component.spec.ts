import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up.component';
import { MatIconModule } from '@angular/material/icon';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let PAYLOAD_MOCK = {
    firstname: 'john',
    lastname: 'doe',
    password: '*****',
    emailAddress: 'johndoe@email.com',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignUpComponent ],
      imports: [ FormsModule, CommonModule, MatIconModule, BrowserAnimationsModule, ReactiveFormsModule, BrowserAnimationsModule, HttpClientTestingModule, MatIconModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should check that password are same or not using isPasswordSame()", () => {
    component.signUpForm.controls['password'].setValue('12345');
    component.signUpForm.controls['confirmPassword'].setValue('123456');
    component.isPasswordSame();
    fixture.detectChanges();
    expect(component.passwordError).toBeDefined();
    expect(component.passwordError).toEqual('Passwords do not match');
    component.signUpForm.controls['password'].setValue('123456789');
    component.signUpForm.controls['confirmPassword'].setValue('123456789');
    component.isPasswordSame();
    fixture.detectChanges();
    expect(component.passwordError).toEqual('');
  })

  it("should contruct a payload using constructPayload()", () => {
    component.signUpForm.controls['firstname'].setValue('john');
    component.signUpForm.controls['lastname'].setValue('doe');
    component.signUpForm.controls['password'].setValue('*****');
    component.signUpForm.controls['email'].setValue('johndoe@email.com');
    fixture.detectChanges();
    let payload = component.constructPayload();
    expect(payload).toBeTruthy();
    expect(payload).toEqual(PAYLOAD_MOCK);
  })
});
