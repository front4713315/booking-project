import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Destinations } from 'src/app/Interfaces/destination.interface';
import { FlightsService } from 'src/app/Services/Flights/flights.service';
import { FlightInformation } from 'src/app/Models/flight-Information.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';

@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss'],
})
export class FlightDetailsComponent implements OnInit {
  user: string = '';
  countryList: Destinations[] = [];
  flightInformation: FlightInformation[] = [];
  flightInformationFiltered: FlightInformation[] = [];
  flightInformationForm: FormGroup = this.formBuilder.group({
    flightSource: new FormControl(null, [Validators.required]),
    flightDestination: new FormControl(null, [Validators.required]),
  });

  constructor(private flightsService: FlightsService, private formBuilder: FormBuilder, private router: Router, private authenticationService: AuthenticationService) {}

  ngOnInit(): void {
    this.flightsService.getDestination().subscribe((data) => {
      this.countryList = data;
    });
    this.user=this.authenticationService.getTokenDetails('name');
  }

  disableElement(inputName: string, countryDetails: Destinations): boolean {
    return this.flightInformationForm.controls[inputName].value === countryDetails['country'] ? true : false;
  }

  canBook(availability: number) {
    return availability <= 0 ? false : true;
  }

  selectFlight(flightId: number | undefined) {
    this.router.navigate(['new-reservation/' + flightId])
  }

  searchFlight() {
    this.flightsService
    .getFlightInformationByCriteria(this.flightInformationForm.controls['flightSource'].value, this.flightInformationForm.controls['flightDestination'].value)
    .subscribe(
      data => {
        this.flightInformationFiltered = data;
      }
    )
  }

  clear() {
    this.flightInformationForm.reset();
  }
}
