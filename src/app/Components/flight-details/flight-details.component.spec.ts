import { of } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlightDetailsComponent } from './flight-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlightsService } from 'src/app/Services/Flights/flights.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

describe('FlightDetailsComponent', () => {
  let component: FlightDetailsComponent;
  let fixture: ComponentFixture<FlightDetailsComponent>;
  let element: HTMLElement;
  let flightService: FlightsService;
  let DESTINATION_MOCK_SAME = {
    id: 1,
    airport: "SSR International Airport",
    country: "Mauritius"
  }  
  let DESTINATION_MOCK_DIFFERENT = {
    id: 2,
    airport: "Plaine Corail Airport",
    country: "Rodrigues"
  };
  let ALL_DESTINATIONS = [
    {
      id: 1,
      airport: "SSR International Airport",
      country: "Mauritius"
    },
    {
      id: 2,
      airport: "Plaine Corail Airport",
      country: "Rodrigues"
    },
    {
      id: 3,
      airport: "Roland Garros Airport",
      country: "Réunion Island"
    }
  ];
  let FLIGHT_INFORMATION = [{
    id: 1,
    name: "M-AIR 172",
    origin: "Mauritius",
    destination: "Rodrigues",
    capacity: 172,
    availability: 88,
    dateOfDeparture: new Date("2024-04-15T05:59:40.101Z"),
    duration: "1 hr 35 minutes",
    distance: "617",
    ticketCostAdult: 9990,
    ticketCostChild: 6990
  }];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightDetailsComponent ],
      imports: [ FormsModule, ReactiveFormsModule, BrowserAnimationsModule, HttpClientTestingModule ]
    }).compileComponents();

    fixture = TestBed.createComponent(FlightDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    flightService = TestBed.inject(FlightsService)
  });

  it('should create flight details component', () => {
    expect(component).toBeTruthy();
  });

  it("should retrieve all the destinations available in ngOnInit when calling getDestination()", fakeAsync( () => {
    let spy = spyOn(flightService, 'getDestination').and.returnValue(of(ALL_DESTINATIONS));
    component.ngOnInit();
    tick();
    expect(component.countryList).toBeDefined();
    expect(component.countryList.length).toEqual(3)
  }));

  it("Should disable or enable an element in a dropdown based on the destination selected in the other dropdown using disableElement()", () => {
    component.flightInformationForm.controls['flightSource'].setValue('Mauritius');
    let returnValueTrue = component.disableElement('flightSource', DESTINATION_MOCK_SAME);
    expect(returnValueTrue).toEqual(true);
    let returnValueFalse = component.disableElement('flightSource', DESTINATION_MOCK_DIFFERENT);
    expect(returnValueFalse).toEqual(false);
  })

  it("should return true or false based on availability to state if booking is possible or not using canBook()", () => {
    expect(component.canBook(0)).toEqual(false)
    expect(component.canBook(200)).toEqual(true)
  });

  it('shoud clear all values from the formGroup when clear() is executed', () => {
    component.flightInformationForm.controls['flightSource'].setValue('Mauritius');
    component.flightInformationForm.controls['flightDestination'].setValue('Rodrigues');
    component.clear();
    expect(component.flightInformationForm.controls['flightSource'].value).toBe(null);
    expect(component.flightInformationForm.controls['flightDestination'].value).toBe(null);
  });

  it("should retrieve a list of all flights using searchFlight()", fakeAsync( () => {
      let spy = spyOn(flightService, "getFlightInformationByCriteria").and.returnValue(of(FLIGHT_INFORMATION));
      component.searchFlight();
      tick();
      expect(component.flightInformationFiltered).toBeDefined()
      expect(component.flightInformationFiltered.length).toEqual(1)
  }));

  it('shoud check the title of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #user');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain('Welcome');
  });

  it('shoud check the subtitle of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #hint');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain('Search for a flight:');
  });

  it("should check the headers and rows of the table which displaythe flights available", (done: DoneFn) => {
    component.flightInformationFiltered = FLIGHT_INFORMATION;
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      fixture.detectChanges();
      let tableRows = fixture.nativeElement.querySelectorAll('tr');
      expect(tableRows.length).toBe(2);
      expect(tableRows[0].cells[0].innerHTML).toBe('S.N');
      expect(tableRows[0].cells[1].innerHTML).toBe('Flight Name');
      expect(tableRows[0].cells[2].innerHTML).toBe('Date of departure');
      expect(tableRows[0].cells[3].innerHTML).toBe('Seats available');
      expect(tableRows[1].cells[0].innerHTML).toBe('1');
      expect(tableRows[1].cells[1].innerHTML).toBe('M-AIR 172');
      expect(tableRows[1].cells[2].innerHTML).toBe('15 Apr 2024');
      expect(tableRows[1].cells[3].innerHTML).toBe('88');
      done();
    })
  });

  it("should assign some values in the departure and destination dropdown", () => {
    component.flightInformationForm.controls['flightSource'].setValue('Mauritius')
    component.flightInformationForm.controls['flightDestination'].setValue('Rodrigues')
    fixture.detectChanges()
    expect(component.flightInformationForm.controls['flightSource'].value).toEqual('Mauritius');
    expect(component.flightInformationForm.controls['flightDestination'].value).toEqual('Rodrigues')
  });
});
