import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in.component';
import { MatIconModule } from '@angular/material/icon';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;
  let element: HTMLElement;
  let authenticationService: AuthenticationService;
  let SIGN_IN_RESPONSE_MOCK = {
    id: 1,
    email: "johndoe@email.com",
    statusCode: 200,
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdG5hbWUiOiJqb2huIiwibGFzdG5hbWUiOiJkb2UiLCJpc0xvZ2dlZEluIjp0cnVlLCJlbWFpbCI6ImpvaG5kb2VAZW1haWwuY29tIn0.DyfiZ1nFjHdnFtkaVfiprbXt7pkpB8blwF5AETtFWq4"
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignInComponent ],
      imports: [ FormsModule, ReactiveFormsModule, BrowserAnimationsModule, CommonModule, BrowserAnimationsModule, HttpClientTestingModule, MatIconModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create sign in component', () => {
    expect(component).toBeTruthy();
  });

  it("should check elements in the component", () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.container .container-input .icon')
    expect(element).toBeTruthy()
    element = fixture.nativeElement.querySelector('.container .container-input .input-bar')
    expect(element).toBeTruthy()
    element = fixture.nativeElement.querySelector('.container .container-btn')
    expect(element).toBeTruthy()
    element = fixture.nativeElement.querySelector('.container .container-btn')
    expect(element).toBeTruthy()
    element = fixture.nativeElement.querySelector('.container .container-btn .disabled-btn')
    expect(element).toBeTruthy()
    let submitEl = fixture.debugElement;
    expect(submitEl.nativeElement.querySelector('button').disabled).toBeTruthy();
  })

  it("should enter some values in the input", () => {
    component.signInForm.controls['email'].setValue('johndoe@email.com')
    component.signInForm.controls['password'].setValue('111')
    fixture.detectChanges()
    expect(component.signInForm.controls['email'].value).toEqual('johndoe@email.com')
    expect(component.signInForm.controls['password'].value).toEqual('111')
    let submitEl = fixture.debugElement;
    expect(submitEl.nativeElement.querySelector('button').disabled).toBeFalsy();
  });  
});
