import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
  loginError: any;
  signInForm: FormGroup = this.formBuilder.group({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(1)]),
  });

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) {}

  signIn() {
    this.loginError = '';
    if (this.signInForm.valid) {
      this.authService.getUserLogin().subscribe(
        (data: any) => {
          sessionStorage.setItem('token', data['token']);
          if (data['statusCode'] == 200 && this.authService.getTokenDetails('isLoggedIn') && data['email'] === this.signInForm.controls['email'].value) {
            this.signInForm.reset();
            this.router.navigate(['flight-details']);
        } 
        else {
          this.loginError = 'Invalid Credentials. Contact support team!';
          sessionStorage.removeItem('token');
          this.signInForm.reset();
          this.router.navigate(['home']);
        }
      });
    } 
    else {
      this.loginError = 'Invalid Credential. Check your email address & password!';
    }
  }

  clear() {
    this.loginError = '';
  }
}
