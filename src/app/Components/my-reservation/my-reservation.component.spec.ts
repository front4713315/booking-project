import { DistancePipe } from 'src/app/Pipes/Distance/distance.pipe';
import { MyReservationComponent } from './my-reservation.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { of } from 'rxjs';import { MoneyPipe } from 'src/app/Pipes/Money/money.pipe';
import { ReservationService } from 'src/app/Services/Reservation/reservation.service';

describe('MyReservationComponent', () => {
  let component: MyReservationComponent;
  let fixture: ComponentFixture<MyReservationComponent>;
  let element: HTMLElement;
  let reservationService: ReservationService;
  let RESERVATION_MOCK_DATA = [
    {
      id: 9,
      bookedBy: "john doe",
      countryOfArrival: "Réunion Island",
      countryOfDeparture: "Rodrigues",
      dateOfBooking: new Date("2024-04-08T23:01:44.003Z"),
      dateOfDeparture: new Date("2024-04-10T01:29:17.101Z"),
      email: "johndoe@email.com",
      flightId: "3",
      flightName: "M-AIR 108",
      numberOfSeatsBooked: {
        adult: 1,
        child: 1
      },
      "totalCost": 29190
    }
  ];
  let RESERVATION_MOCK = [
    {
      id: 1,
      email: "johndoe@email.com",
      bookedBy: "john doe",
      dateOfBooking: "2024-02-24T05:59:40.101Z",
      flightId: 1,
      flightName: "M-AIR 172",
      numberOfSeatsBooked: {
        adult: 1,
        child: 2
      },
      totalCost: 23970,
      countryOfDeparture: "Mauritius",
      countryOfArrival: "Rodrigues",
      dateOfDeparture: "2024-03-05T06:21:40.101Z"
    },
  ];
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MyReservationComponent, DistancePipe, MoneyPipe],
      imports: [HttpClientTestingModule],
      providers: [ReservationService]
    }).compileComponents();

    fixture = TestBed.createComponent(MyReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    reservationService = TestBed.inject(ReservationService);
  });

  it('should create my reservations component', () => {
    expect(component).toBeTruthy();
  });

  it("should retrieve all the reservation of the user in ngOnInit", fakeAsync( () => {    
    let spy = spyOn(reservationService, 'getMyReservation').and.returnValue(of(RESERVATION_MOCK_DATA));
    component.ngOnInit();
    tick();
    expect(component.myReservationDetails).toBeDefined();
    expect(component.myReservationDetails.length).toEqual(1)
  }));

  it('shoud check the title of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #user');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain('Welcome back');
  });

  it('shoud check the subtitle of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #hint');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain(component.hint);
  });

  it('should check that div for each tickets exist', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('#ticket-container');
    expect(element).toBeTruthy()
  })

  it("should check the information displayed for a ticket", (done: DoneFn) => {
    component.myReservationDetails = RESERVATION_MOCK;
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      fixture.detectChanges();
      let tableRows = fixture.nativeElement.querySelectorAll('tr');
      expect(tableRows.length).toBe(4);
      expect(tableRows[0].cells[0].innerHTML).toBe('Flight Name(ID): M-AIR 172 (1)');
      expect(tableRows[0].cells[1].innerHTML).toBe('Date of departure: 05 Mar 2024 10:03:10');
      expect(tableRows[1].cells[0].innerHTML).toBe('Country of departure: Mauritius');
      expect(tableRows[1].cells[1].innerHTML).toBe('Country of arrival: Rodrigues');
      expect(tableRows[2].cells[0].innerHTML).toBe('Number of seats: Adults (1) - Children (2)');
      expect(tableRows[3].cells[0].innerHTML).toBe('Cost of tickets: Rs 23970');
      done();
    })
  });
});
