import { Component, OnInit } from '@angular/core';
import { Reservation } from 'src/app/Models/reservation.model';
import { ReservationService } from 'src/app/Services/Reservation/reservation.service';

@Component({
  selector: 'app-my-reservation',
  templateUrl: './my-reservation.component.html',
  styleUrls: ['./my-reservation.component.scss'],
})
export class MyReservationComponent implements OnInit {
  myReservationDetails: Reservation[] = [];
  user: string = '';
  hint: string = 'Here are your past booking with us:';

  constructor(private reservationService: ReservationService) {}

  ngOnInit() {
    this.reservationService.getMyReservation().subscribe(
      data => {
        this.myReservationDetails = data;
        this.user = this.myReservationDetails[0]['bookedBy'];
        this.myReservationDetails.sort(function (a, b) {
          return (new Date(b['dateOfBooking']).valueOf() - new Date(a['dateOfBooking']).valueOf());
        });
      }
    );
  }
}
