import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  redirectionLink1: string = 'My reservations';
  redirectionLink2: string = 'Flight details';
  
  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  logout(): void {
    sessionStorage.removeItem('token');
    this.router.navigate(['/home']);
  }

  isLoggedIn(): boolean {
    return this.authenticationService.getTokenDetails('isLoggedIn') ? true : false;
  }
}