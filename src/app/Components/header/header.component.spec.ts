import { HeaderComponent } from './header.component';
import { MatIconModule } from '@angular/material/icon';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MyReservationComponent } from '../my-reservation/my-reservation.component';
import { FlightDetailsComponent } from '../flight-details/flight-details.component';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let element: HTMLElement;
  let authenticationService: AuthenticationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderComponent, MyReservationComponent, FlightDetailsComponent ],
      imports: [ MatIconModule, HttpClientTestingModule ],
      providers: [ AuthenticationService ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the header component', () => {
    expect(component).toBeTruthy();
  });

  it('should return true if user is logged in when isLoggedIn() is executed', () => {
    authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    spyOn(authenticationService, "getTokenDetails").and.returnValue(true)
    let loggedIn = component.isLoggedIn();
    expect(loggedIn).toEqual(true);
  });

  it('should return false if user is not logged in when isLoggedIn() is executed', () => {
    authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    spyOn(authenticationService, "getTokenDetails").and.returnValue(false)
    let loggedIn = component.isLoggedIn();
    expect(loggedIn).toEqual(false);
  });
});
