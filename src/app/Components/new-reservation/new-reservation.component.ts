import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Reservation } from 'src/app/Models/reservation.model';
import { FlightsService } from 'src/app/Services/Flights/flights.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ReservationService } from 'src/app/Services/Reservation/reservation.service';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';
import { FlightInformation } from 'src/app/Models/flight-Information.model';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss'],
})
export class NewReservationComponent {
  flightInformation: FlightInformation[] = [];
  today = new Date();
  bookedBy: string = '';
  email: string = '';
  hint: string = 'Enter your flight details:';
  bookingForm: FormGroup = this.formBuilder.group({
    adultTicket: new FormControl( '', [Validators.required]),
    childTicket: new FormControl( '', [Validators.required]),
    adultPrice: new FormControl({ value: 0, disabled: true }, [Validators.required, Validators.min(0)]),
    childPrice: new FormControl({ value: 0, disabled: true }, [Validators.required, Validators.min(0)]),
    totalPrice: new FormControl({ value: 0, disabled: true }, [Validators.required, Validators.min(0)]),
  });

  ngOnInit(): void {
    this.flightService
      .getFlightInformationById(this.route.snapshot.paramMap.get('flightId'))
      .subscribe((data) => {
        this.flightInformation.push(data);
        this.getUserInfo();
      }
    );
    this.bookingForm.controls['adultTicket'].valueChanges
      .subscribe(() => {
        if(this.flightInformation[0]) {
          this.bookingForm.controls['adultPrice'].setValue(this.flightInformation[0]['ticketCostAdult'] * this.bookingForm.controls['adultTicket'].value);
          this.bookingForm.controls['totalPrice'].setValue(this.bookingForm.controls['adultPrice'].value + this.bookingForm.controls['childPrice'].value);
        }        
      }
    );
    this.bookingForm.controls['childTicket'].valueChanges
      .subscribe(() => {
        if(this.flightInformation[0]) {
          this.bookingForm.controls['childPrice'].setValue(this.flightInformation[0]['ticketCostChild'] * this.bookingForm.controls['childTicket'].value);
          this.bookingForm.controls['totalPrice'].setValue(this.bookingForm.controls['adultPrice'].value + this.bookingForm.controls['childPrice'].value);
        }        
      }
    );
    
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private flightService: FlightsService,
    private authenticationService: AuthenticationService,
    private reservationService: ReservationService,
    private formBuilder: FormBuilder
  ) { }

  getUserInfo() {
    this.bookedBy = this.authenticationService.getTokenDetails('name');
    this.email = this.authenticationService.getTokenDetails('email');
  }

  constructPayloadModel(flightInformation: FlightInformation[]): Reservation {
    let payload = {
      bookedBy: this.bookedBy,
      countryOfArrival: flightInformation[0]['destination'],
      countryOfDeparture: flightInformation[0]['origin'],
      dateOfBooking: new Date(),
      dateOfDeparture: flightInformation[0]['dateOfDeparture'],
      email: this.email,
      flightId: flightInformation[0]['id'],
      flightName: flightInformation[0]['name'],
      numberOfSeatsBooked: {
        adult: this.bookingForm.controls['adultTicket'].value,
        child: this.bookingForm.controls['childTicket'].value,
      },
      totalCost: this.bookingForm.controls['totalPrice'].value,
    };
    return payload;
  }

  confirmBooking() {
    this.flightInformation[0].availability = this.flightInformation[0].availability - (this.bookingForm.controls['adultTicket'].value + this.bookingForm.controls['adultTicket'].value);
    this.reservationService.postNewReservation(this.constructPayloadModel(this.flightInformation)).subscribe(
      data => {
        if(data) {
          this.flightService.putFlightInformation(this.flightInformation[0].id ,this.flightInformation[0]).subscribe(
            result => {
              if(result) {
                this.router.navigate(['my-reservation'])
              }              
            }
          )
        }        
      }
    )
  }

  clearForm() {
    this.bookingForm.reset();
    this.router.navigate(['flight-details']);
  }
}
