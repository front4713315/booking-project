import { of } from 'rxjs';
import { MoneyPipe } from 'src/app/Pipes/Money/money.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DistancePipe } from 'src/app/Pipes/Distance/distance.pipe';
import { NewReservationComponent } from './new-reservation.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlightsService } from 'src/app/Services/Flights/flights.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { FlightDetailsComponent } from '../flight-details/flight-details.component';
import { ReservationService } from 'src/app/Services/Reservation/reservation.service';
import { AuthenticationService } from 'src/app/Services/Authentication/authentication.service';

describe('NewReservationComponent', () => {
  let component: NewReservationComponent;
  let fixture: ComponentFixture<NewReservationComponent>;
  let element: HTMLElement;
  let flightService: FlightsService;
  let reservationService: ReservationService;
  let authenticationService: AuthenticationService;
  let FLIGHT_INFORMATION_MOCK = {
    id: 1,
    name: "M-AIR 172",
    origin: "Mauritius",
    destination: "Rodrigues",
    capacity: 172,
    availability: 88,
    dateOfDeparture: "2024-04-15T05:59:40.101Z",
    duration: "1 hr 35 minutes",
    distance: "617",
    ticketCostAdult: 9990,
    ticketCostChild: 6990
  }
  let FLIGHT_INFORMATION = [{
    id: 1,
    name: "M-AIR 172",
    origin: "Mauritius",
    destination: "Rodrigues",
    capacity: 172,
    availability: 88,
    dateOfDeparture: "2024-04-15T05:59:40.101Z",
    duration: "1 hr 35 minutes",
    distance: "617",
    ticketCostAdult: 9990,
    ticketCostChild: 6990
  }];
  let RESERVATION_MOCK = [
    {
      id: 1,
      email: "johndoe@email.com",
      bookedBy: "john doe",
      dateOfBooking: "2024-02-24T05:59:40.101Z",
      flightId: 1,
      flightName: "M-AIR 172",
      numberOfSeatsBooked: {
        adult: 1,
        child: 2
      },
      totalCost: 23970,
      countryOfDeparture: "Mauritius",
      countryOfArrival: "Rodrigues",
      dateOfDeparture: "2024-03-05T06:21:40.101Z"
    },
  ];
  let PAYLOAD_MOCK = {
    email: "johndoe@email.com",
    bookedBy: "john doe",
    dateOfBooking: new Date(),
    flightId: 1,
    flightName: "M-AIR 172",
    numberOfSeatsBooked: {
      adult: 1,
      child: 2
    },
    totalCost: 23970,
    countryOfDeparture: "Mauritius",
    countryOfArrival: "Rodrigues",
    dateOfDeparture: "2024-04-15T05:59:40.101Z"
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewReservationComponent, DistancePipe, MoneyPipe ],
      imports: [ FormsModule, ReactiveFormsModule, BrowserAnimationsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([{path: 'flight-details', component: FlightDetailsComponent}]) ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    flightService = TestBed.inject(FlightsService);
    reservationService = TestBed.inject(ReservationService);
    authenticationService = TestBed.inject(AuthenticationService);
  });

  it('should create the new reservation component', () => {
    expect(component).toBeTruthy();
  });

  it("should retrieve all the flight's information available in ngOnInit when calling getFlightInformationById()", fakeAsync( () => {
    let spy = spyOn(flightService, 'getFlightInformationById').and.returnValue(of(FLIGHT_INFORMATION_MOCK));
    component.ngOnInit();
    tick();
    expect(component.flightInformation).toBeDefined();
    expect(component.flightInformation.length).toEqual(1)
  }));

  it("should retrieve the value of bookedBy when getUserInfo() is called from ngOnInit", () => {
    spyOn(authenticationService, 'getTokenDetails').and.returnValue('john doe')
    component.getUserInfo();
    expect(component.bookedBy).toEqual('john doe')
  });

  it("should retrieve the value of email when getUserInfo() is called from ngOnInit", () => {
    spyOn(authenticationService, 'getTokenDetails').and.returnValue('johndoe@email.com')
    component.getUserInfo();
    expect(component.bookedBy).toEqual('johndoe@email.com')
  })

  it("should construct the payload", () => {
    component.bookedBy = 'john doe';
    component.email = 'johndoe@email.com';
    component.bookingForm.controls['adultTicket'].setValue(1);
    component.bookingForm.controls['childTicket'].setValue(2);
    component.bookingForm.controls['adultPrice'].setValue(9990);
    component.bookingForm.controls['childPrice'].setValue(6990);
    component.bookingForm.controls['totalPrice'].setValue(23970);
    component.flightInformation = FLIGHT_INFORMATION;
    let payload = component.constructPayloadModel(FLIGHT_INFORMATION);
    expect(payload).toBeTruthy();
    expect(payload.bookedBy).toEqual(PAYLOAD_MOCK.bookedBy);
    expect(payload.countryOfArrival).toEqual(PAYLOAD_MOCK.countryOfArrival);
    expect(payload.countryOfDeparture).toEqual(PAYLOAD_MOCK.countryOfDeparture);
    expect(payload.dateOfDeparture).toEqual(PAYLOAD_MOCK.dateOfDeparture);
    expect(payload.email).toEqual(PAYLOAD_MOCK.email);
    expect(payload.flightId).toEqual(PAYLOAD_MOCK.flightId);
    expect(payload.flightName).toEqual(PAYLOAD_MOCK.flightName);
    expect(payload.numberOfSeatsBooked.adult).toEqual(PAYLOAD_MOCK.numberOfSeatsBooked.adult);
    expect(payload.numberOfSeatsBooked.child).toEqual(PAYLOAD_MOCK.numberOfSeatsBooked.child);
    expect(payload.totalCost).toEqual(PAYLOAD_MOCK.totalCost);
  })

  it("should assign some values in the departure and destination dropdown", () => {
    component.flightInformation = FLIGHT_INFORMATION;
    component.bookingForm.controls['adultTicket'].setValue(1)
    component.bookingForm.controls['childTicket'].setValue(2)
    component.bookingForm.controls['adultPrice'].setValue(9990)
    component.bookingForm.controls['childPrice'].setValue(6990)
    fixture.detectChanges()
    expect(component.bookingForm.controls['adultTicket'].value).toEqual(1);
    expect(component.bookingForm.controls['childTicket'].value).toEqual(2)
    expect(component.bookingForm.controls['adultPrice'].value).toEqual(9990)
    expect(component.bookingForm.controls['childPrice'].value).toEqual(6990)
    expect(component.bookingForm.controls['totalPrice'].value).toEqual(23970)
  })

  it('shoud clear all values from the formGroup when clear() is executed', () => {
    component.bookingForm.controls['adultTicket'].setValue(2);
    component.bookingForm.controls['childTicket'].setValue(5);
    component.clearForm();
    expect(component.bookingForm.controls['adultTicket'].value).toBe(null);
    expect(component.bookingForm.controls['childTicket'].value).toBe(null);

  });

  it('shoud check the title of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #user');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain('Welcome back');
  });

  it('shoud check the subtitle of the page', () => {
    fixture.detectChanges();
    element = fixture.nativeElement.querySelector('.page-header #hint');
    expect(element).toBeTruthy()
    expect(element.textContent).toContain(component.hint);
  });

  it("should check the headers and rows of the table which displaythe flights available", (done: DoneFn) => {
    component.flightInformation = FLIGHT_INFORMATION;
    component.bookedBy = 'john doe';
    component.email = 'johndoe@email.com';
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      fixture.detectChanges();
      let tableRows = fixture.nativeElement.querySelectorAll('tr');
      expect(tableRows.length).toBe(15);
      expect(tableRows[0].cells[0].innerHTML).toBe('Flight Name(ID):');
      expect(tableRows[0].cells[1].innerHTML).toBe('M-AIR 172 (1)')
      expect(tableRows[3].cells[0].innerHTML).toBe('Flight capacity:');
      expect(tableRows[3].cells[1].innerHTML).toBe('172');
      expect(tableRows[4].cells[0].innerHTML).toBe('Seats Available:');
      expect(tableRows[4].cells[1].innerHTML).toBe('88');
      expect(tableRows[5].cells[0].innerHTML).toBe('From:');
      expect(tableRows[5].cells[1].innerHTML).toBe('Mauritius');
      expect(tableRows[6].cells[0].innerHTML).toBe('To:');
      expect(tableRows[6].cells[1].innerHTML).toBe('Rodrigues');
      expect(tableRows[8].cells[0].innerHTML).toBe('Distance:');
      expect(tableRows[8].cells[1].innerHTML).toBe('617 km');
      expect(tableRows[9].cells[0].innerHTML).toBe('Duration of flight:');
      expect(tableRows[9].cells[1].innerHTML).toBe('1 hr 35 minutes');
      expect(tableRows[10].cells[0].innerHTML).toBe('Booked by:');
      expect(tableRows[10].cells[1].innerHTML).toBe('john doe');
      expect(tableRows[11].cells[0].innerHTML).toBe('Email:');
      expect(tableRows[11].cells[1].innerHTML).toBe('johndoe@email.com');
      done();
    })
  })
});
