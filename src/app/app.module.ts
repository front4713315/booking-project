import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './Auth-Guards/auth.guard';
import { MoneyPipe } from './Pipes/Money/money.pipe';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { DistancePipe } from './Pipes/Distance/distance.pipe';
import { HomeComponent } from './Components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlightsService } from './Services/Flights/flights.service';
import { HeaderComponent } from './Components/header/header.component';
import { SignInComponent } from './Components/sign-in/sign-in.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReservationService } from './Services/Reservation/reservation.service';
import { AuthenticationService } from './Services/Authentication/authentication.service';
import { MyReservationComponent } from './Components/my-reservation/my-reservation.component';
import { FlightDetailsComponent } from './Components/flight-details/flight-details.component';
import { NewReservationComponent } from './Components/new-reservation/new-reservation.component';

@NgModule({
  declarations: [
    AppComponent,
    DistancePipe,
    FlightDetailsComponent,
    HeaderComponent,
    HomeComponent,
    NewReservationComponent,
    MoneyPipe,
    MyReservationComponent,
    SignInComponent,
    SignUpComponent,     
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatSelectModule
  ],
  providers: [
    ReservationService,
    AuthenticationService,
    FlightsService,
    AuthGuard,
  ],
  bootstrap: [AppComponent],
  exports: [DistancePipe, MoneyPipe],
})
export class AppModule {}